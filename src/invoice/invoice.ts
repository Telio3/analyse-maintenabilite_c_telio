import { Car } from "./classes/car";
import { Customer } from "./classes/customer";
import { PriceCode } from "./classes/priceCode";
import { Rental } from "./classes/rental";

// Exemples d'utilisation :
class RegularPriceCode extends PriceCode {
    public getAmount(daysRented: number): number {
        let amount = 5000 + daysRented * 9500;
        if (daysRented > 5) {
            amount -= (daysRented - 2) * 10000;
        }
        return amount;
    }
}

class NewModelPriceCode extends PriceCode {
    public getAmount(daysRented: number): number {
        let amount = 9000 + daysRented * 15000;
        if (daysRented > 3) {
            amount -= (daysRented - 2) * 10000;
        }
        return amount;
    }
}

const car1 = new Car("Car 1", new RegularPriceCode());
const car2 = new Car("Car 2", new NewModelPriceCode());

const rental1 = new Rental(car1, 3);
const rental2 = new Rental(car2, 2);

const customer = new Customer("John Doe");
customer.addRental(rental1);
customer.addRental(rental2);

console.log(customer.statement());

import { PriceCode } from "./priceCode";

export class Car {
    private readonly _title: string;
    private _priceCode: PriceCode;

    constructor(title: string, priceCode: PriceCode) {
        this._title = title;
        this._priceCode = priceCode;
    }

    public getPriceCode(): PriceCode {
        return this._priceCode;
    }

    public setPriceCode(arg: PriceCode): void {
        this._priceCode = arg;
    }

    public getTitle(): string {
        return this._title;
    }
}

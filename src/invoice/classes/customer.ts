import { Rental } from "./rental";
import { StatementType } from "./statement.type";

export class Customer {
    private readonly _name: string;  // Nom du client
    private readonly _rentals: Rental[] = [];  // Liste des locations associées au client

    constructor(name: string) {
        this._name = name;
    }

    // Récupère le nom du client
    public getName(): string {
        return this._name;
    }

    // Ajoute une nouvelle location à la liste des locations
    public addRental(rental: Rental): void {
        this._rentals.push(rental);
    }

    // Génère un relevé des locations dans le format spécifié (ASCII par défaut, JSON en option)
    public statement(format: StatementType = StatementType.ASCII): string {
        switch (format) {
            case StatementType.ASCII:
                return this._statementAsAscii();
            case StatementType.JSON:
                return JSON.stringify(this._statementAsJSON(), null, 2);
            default:
                throw new Error(`Unsupported statement format: ${format}`);
        }
    }

    // Génère un relevé sous forme de chaîne ASCII
    private _statementAsAscii(): string {
        let result = `Rental Record for ${this.getName()}\n`;  // En-tête du relevé
        for (const rental of this._rentals) {
            result += `\t${rental.getCar().getTitle()}\t${(rental.getAmount() / 100).toFixed(1)}\n`;  // Détails de chaque location
        }
        const totalAmount = this._calculateTotalAmount();
        result += `Amount owed is ${(totalAmount / 100).toFixed(1)}\n`;  // Montant total dû
        result += `You earned ${this._calculateFrequentRenterPoints(totalAmount)} frequent renter points\n`;  // Points de fidélité gagnés
        return result;
    }

    // Génère un relevé sous forme de JSON
    private _statementAsJSON(): Record<string, any> {
        const rentalsArray = this._rentals.map(rental => ({
            title: rental.getCar().getTitle(),  // Titre de la voiture
            amount: (rental.getAmount() / 100).toFixed(1),  // Montant dû pour cette location
        }));

        const totalAmount = this._calculateTotalAmount();

        return {
            customerName: this.getName(),  // Nom du client
            rentals: rentalsArray,  // Détails des locations
            totalAmount: (totalAmount / 100).toFixed(1),  // Montant total dû
            frequentRenterPoints: this._calculateFrequentRenterPoints(totalAmount),  // Points de fidélité gagnés
        };
    }

    // Calcule le montant total dû pour toutes les locations
    private _calculateTotalAmount(): number {
        return this._rentals.reduce((total, rental) => total + rental.getAmount(), 0);
    }

    // Calcule le nombre de points de fidélité gagnés pour toutes les locations
    private _calculateFrequentRenterPoints(totalAmount: number): number {
        return Math.floor(totalAmount / 1000);
    }
}

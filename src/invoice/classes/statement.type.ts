export class StatementType {
    public static ASCII: string = 'ascii';
    public static JSON: string = 'json';
}

export abstract class PriceCode {
    public abstract getAmount(daysRented: number): number;
}

import { Car } from "./car";

export class Rental {
    private readonly _car: Car;
    private readonly _daysRented: number;

    constructor(car: Car, daysRented: number) {
        this._car = car;
        this._daysRented = daysRented;
    }

    public getDaysRented(): number {
        return this._daysRented;
    }

    public getCar(): Car {
        return this._car;
    }

    public getAmount(): number {
        return this._car.getPriceCode().getAmount(this._daysRented);
    }
}

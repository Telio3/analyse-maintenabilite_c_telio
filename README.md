# Analyse de la maintenabilité - CORRE Télio

## Introduction

Ce projet est une évaluation de l'analyse de la maintenabilité d'un projet.

## Table des matières

- [Exercice 1](#exercice-1)
    - [Quelles sont les principales sources de complexité dans un système logiciel (sources d’un programme) ?](#exercice-1.1)
    - [Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ?](#exercice-1.2)
    - [Comment comprenez-vous chaque étape de l’heuristique “First make it run, next make it correct, and only after that worry about making it fast” ?](#exercice-1.3)
    - [Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?](#exercice-1.4)
- [Exercice 2](#exercice-2)
    - [Installer et lancer le projet](#installer-et-lancer-le-projet)
    - [Exécuter les tests](#exécuter-les-tests)


## Exercice 1

<div id="exercice-1.1"></div>

### 1. Quelles sont les principales sources de complexité dans un système logiciel (sources d’un programme) ?

Les principales sources de complexité dans un système logiciel incluent :

1. **Complexité essentielle** : Intrinsèque au problème que le logiciel est censé résoudre. Cela inclut les exigences fonctionnelles, les règles métier et les contraintes du domaine.
2. **Complexité accidentelle** : Provient de la solution technique choisie pour résoudre le problème. Elle inclut les choix de conception, l'architecture du logiciel, les bibliothèques et frameworks utilisés, et les détails d'implémentation.
3. **Complexité structurelle** : Résulte de la façon dont le code est organisé et structuré. Une mauvaise modularisation, des dépendances cycliques, et un couplage élevé entre les composants peuvent ajouter à cette complexité.
4. **Complexité comportementale** : Liée au comportement dynamique du système, comme la gestion de l'état, la concurrence, les interactions entre composants et les effets de bord.
5. **Complexité de maintenance** : Due à la difficulté de comprendre, modifier et étendre le code existant. Cela peut être causé par un code mal documenté, des tests insuffisants, et des pratiques de codage inconsistantes.

<div id="exercice-1.2"></div>

### 2. Quel(s) avantage(s) procure le fait de programmer vers une interface et non vers une implémentation ?

**Avantages :**

1. **Découplage** : Permet de séparer l'interface de l'implémentation, ce qui réduit les dépendances directes et facilite les changements de l'implémentation sans affecter les clients.
2. **Flexibilité** : Facilite le remplacement de l'implémentation par une autre, offrant ainsi plus de flexibilité pour améliorer ou modifier le comportement sans impacter les autres parties du système.
3. **Testabilité** : Permet l'utilisation de mocks ou stubs lors des tests, ce qui rend les tests unitaires plus faciles et plus efficaces.
4. **Réutilisabilité** : Les interfaces peuvent être implémentées par plusieurs classes différentes, augmentant ainsi la réutilisabilité du code.
5. **Lisibilité et compréhension** : Clarifie les contrats entre les différentes parties du système, ce qui améliore la lisibilité et la compréhension du code.

**Exemple de code :**

```typescript
interface Animal {
    makeSound(): void;
}

class Dog implements Animal {
    makeSound(): void {
        console.log("Woof woof!");
    }
}

class Cat implements Animal {
    makeSound(): void {
        console.log("Meow!");
    }
}

class Cow implements Animal {
    makeSound(): void {
        console.log("Moo!");
    }
}

const dog: Animal = new Dog();
const cat: Animal = new Cat();
const cow: Animal = new Cow();

dog.makeSound(); // Output: Woof woof!
cat.makeSound(); // Output: Meow!
cow.makeSound(); // Output: Moo!
```

<div id="exercice-1.3"></div>

### 3. Comment comprenez-vous chaque étape de l’heuristique “First make it run, next make it correct, and only after that worry about making it fast” ?

**First make it run (D'abord, faites en sorte que ça fonctionne)** : L'objectif initial est d'avoir une solution fonctionnelle, même si elle est imparfaite. Il s'agit de créer un prototype ou une version de base du logiciel qui répond aux exigences minimales.

**Next make it correct (Ensuite, assurez-vous que ce soit correct)** : Une fois que le logiciel fonctionne, l'étape suivante est de s'assurer qu'il est correct et répond pleinement aux exigences spécifiées. Cela inclut la correction des bugs, l'ajout de tests, et l'amélioration de la précision et de la robustesse du logiciel.

**Only after that worry about making it fast (et seulement après, préoccupez-vous de le rendre rapide)** : La performance doit être optimisée seulement après avoir atteint la fonctionnalité et la correction. Cette étape consiste à identifier les goulots d'étranglement et à optimiser le code pour améliorer l'efficacité et les performances.

**Heuristique dans son ensemble** : Cette heuristique met l'accent sur une approche itérative et incrémentale du développement logiciel. Elle encourage les développeurs à se concentrer d'abord sur la fonctionnalité de base, puis sur la qualité et la robustesse, et enfin sur l'optimisation des performances. Cela permet de s'assurer que le produit est utilisable à chaque étape, tout en améliorant progressivement sa qualité et son efficacité.

<div id="exercice-1.4"></div>

### 4. Quelle méthode ou approche est recommandée pour mener à bien un travail de refactoring ?

**Approche recommandée pour le refactoring :**

1. **Tests unitaires exhaustifs** : Avant de commencer le refactoring, assurez-vous que vous disposez de tests unitaires complets et fiables pour couvrir le comportement existant du code. Cela garantit que les modifications n'introduiront pas de nouveaux bugs.
2. **Refactoring incrémental** : Effectuez des modifications petites et progressives plutôt que des refontes complètes. Cela permet de minimiser les risques et de faciliter la gestion des changements.
3. **Compréhension du code existant** : Avant de modifier le code, comprenez son fonctionnement actuel, ses dépendances et son rôle dans le système.
4. **Automatisation des tests** : Utilisez des outils d'intégration continue pour exécuter automatiquement les tests à chaque modification, garantissant ainsi que les changements n'introduisent pas de régressions.
5. **Documentation et communication** : Documentez les changements de manière claire et communiquez avec les membres de l'équipe pour s'assurer que tout le monde comprend les raisons et les objectifs du refactoring.
6. **Amélioration progressive** : Priorisez les parties du code qui bénéficieraient le plus du refactoring, telles que les zones de code complexes, les modules fréquemment modifiés ou ceux qui posent des problèmes récurrents.

**Exemple de refactoring simple :**

Avant refactoring :
```typescript
function calculateArea(shape: any) {
    if (shape.type === 'circle') {
        return Math.PI * shape.radius * shape.radius;
    } else if (shape.type === 'rectangle') {
        return shape.width * shape.height;
    }
    return 0;
}
```

Après refactoring :
```typescript
interface Shape {
    area(): number;
}

class Circle implements Shape {
    constructor(private radius: number) {}
    
    area(): number {
        return Math.PI * this.radius * this.radius;
    }
}

class Rectangle implements Shape {
    constructor(private width: number, private height: number) {}
    
    area(): number {
        return this.width * this.height;
    }
}

function calculateArea(shape: Shape) {
    return shape.area();
}
```

Ce refactoring améliore la lisibilité, la réutilisabilité et la testabilité du code.

## Exercice 2

### Installer et lancer le projet

```bash
# Cloner le projet
$ git clone https://gitlab.com/Telio3/analyse-maintenabilite_c_telio.git

# Installer les dépendances
$ npm install

# Build le projet
$ npm run build

# Lancer le projet
$ npm start
```

### Exécuter les tests

```bash
$ npm test
```
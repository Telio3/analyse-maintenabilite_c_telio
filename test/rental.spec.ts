import { Car } from "../src/invoice/classes/car";
import { PriceCode } from "../src/invoice/classes/priceCode";
import { Rental } from "../src/invoice/classes/rental";

class TestPriceCode extends PriceCode {
    public getAmount(daysRented: number): number {
        return 1000 * daysRented;
    }
}

describe('Rental', () => {
    it('should return the correct days rented', () => {
        const car = new Car('Test Car', new TestPriceCode());
        const rental = new Rental(car, 3);
        expect(rental.getDaysRented()).toBe(3);
    });

    it('should return the correct car', () => {
        const car = new Car('Test Car', new TestPriceCode());
        const rental = new Rental(car, 3);
        expect(rental.getCar()).toBe(car);
    });

    it('should return the correct amount', () => {
        const car = new Car('Test Car', new TestPriceCode());
        const rental = new Rental(car, 3);
        expect(rental.getAmount()).toBe(3000);
    });
});

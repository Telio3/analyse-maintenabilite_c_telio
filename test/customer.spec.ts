import { Car } from "../src/invoice/classes/car";
import { Customer } from "../src/invoice/classes/customer";
import { PriceCode } from "../src/invoice/classes/priceCode";
import { Rental } from "../src/invoice/classes/rental";
import { StatementType } from "../src/invoice/classes/statement.type";

class TestPriceCode extends PriceCode {
    public getAmount(daysRented: number): number {
        return 1000 * daysRented;
    }
}

describe('Customer', () => {
    it('should return the correct name', () => {
        const customer = new Customer('John Doe');
        expect(customer.getName()).toBe('John Doe');
    });

    it('should add rentals and generate ASCII statement', () => {
        const customer = new Customer('John Doe');
        const car = new Car('Test Car', new TestPriceCode());
        const rental = new Rental(car, 3);

        customer.addRental(rental);

        const statement = customer.statement(StatementType.ASCII);
        expect(statement).toContain('Rental Record for John Doe');
        expect(statement).toContain('Test Car\t30.0');
        expect(statement).toContain('Amount owed is 30.0');
        expect(statement).toContain('You earned 3 frequent renter points');
    });

    it('should generate JSON statement', () => {
        const customer = new Customer('John Doe');
        const car = new Car('Test Car', new TestPriceCode());
        const rental = new Rental(car, 3);

        customer.addRental(rental);

        const statement = customer.statement(StatementType.JSON);
        const expectedJSON = {
            customerName: 'John Doe',
            rentals: [{ title: 'Test Car', amount: '30.0' }],
            totalAmount: '30.0',
            frequentRenterPoints: 3,
        };

        expect(JSON.parse(statement)).toEqual(expectedJSON);
    });
});

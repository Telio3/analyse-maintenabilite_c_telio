import { PriceCode } from "../src/invoice/classes/priceCode";

class RegularPriceCode extends PriceCode {
    public getAmount(daysRented: number): number {
        let amount = 5000 + daysRented * 9500;
        if (daysRented > 5) {
            amount -= (daysRented - 2) * 10000;
        }
        return amount;
    }
}

class NewModelPriceCode extends PriceCode {
    public getAmount(daysRented: number): number {
        let amount = 9000 + daysRented * 15000;
        if (daysRented > 3) {
            amount -= (daysRented - 2) * 10000;
        }
        return amount;
    }
}

describe('PriceCode', () => {
    it('should calculate the amount for RegularPriceCode', () => {
        const priceCode = new RegularPriceCode();
        expect(priceCode.getAmount(3)).toBe(33500);
        expect(priceCode.getAmount(6)).toBe(22000);
    });

    it('should calculate the amount for NewModelPriceCode', () => {
        const priceCode = new NewModelPriceCode();
        expect(priceCode.getAmount(2)).toBe(39000);
        expect(priceCode.getAmount(4)).toBe(49000);
    });
});

import { Car } from "../src/invoice/classes/car";
import { PriceCode } from "../src/invoice/classes/priceCode";

class TestPriceCode extends PriceCode {
    public getAmount(daysRented: number): number {
        return 1000 * daysRented;
    }
}

describe('Car', () => {
    it('should return the correct title', () => {
        const car = new Car('Test Car', new TestPriceCode());
        expect(car.getTitle()).toBe('Test Car');
    });

    it('should return the correct price code', () => {
        const priceCode = new TestPriceCode();
        const car = new Car('Test Car', priceCode);
        expect(car.getPriceCode()).toBe(priceCode);
    });

    it('should allow setting a new price code', () => {
        const priceCode1 = new TestPriceCode();
        const priceCode2 = new TestPriceCode();
        const car = new Car('Test Car', priceCode1);
        car.setPriceCode(priceCode2);
        expect(car.getPriceCode()).toBe(priceCode2);
    });
});
